package com.example.jonesdanica.socialmediasignin.entities;

/**
 * Created by danica12 on 7/22/2016.
 */
public class FbProfile {
    private String name;
    private String birthday;
    private String emailAddress;
    private String gender;
    private String address;

    public FbProfile() {}

    public FbProfile(String name, String birthday, String emailAddress, String gender, String address) {
        this.name = name;
        this.birthday = birthday;
        this.emailAddress = emailAddress;
        this.gender = gender;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "FbProfile{" +
                "name='" + name + '\'' +
                ", birthday='" + birthday + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
