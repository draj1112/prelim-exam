package com.example.jonesdanica.socialmediasignin.apis;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.jonesdanica.socialmediasignin.entities.FbProfile;
import com.example.jonesdanica.socialmediasignin.utils.HttpUtils;

/**
 * Created by danica12 on 7/22/2016.
 */
public class FbAPI {

        public static final String BASE_URL     = "http://graph.facebook.com";
        //public static final String IMG_BASE_URL = "http://openweathermap.org/img/w/";

        public static final int SUCCESS_CODE = 200;

        /*public static final String PARAM_QUERY = "q";
        public static final String PARAM_MODE    = "mode";
        public static final String PARAM_UNITS   = "units";
        public static final String PARAM_API_KEY = "appId";*/

        private static final String OWM_CODE      = "code";
        private static final String OWM_ID        = "id";
        private static final String OWM_BIRTHDAY  = "birthday";
        private static final String OWM_EMAIL     = "email";
        private static final String OWM_GENDER    = "gender";
        private static final String OWM_FNAME     = "first_name";
        private static final String OWM_LNAME     = "last_name";

        public static FbProfile getProfile(Uri uri, @NonNull String requestMethod) {
            String json = HttpUtils.getResponse(uri, requestMethod);

            if (TextUtils.isEmpty(json)) {
                return null;
            }

            // Here we will now parse the json response and convert it into a Weather object.

            final String id;
            final String birthday;
            final String email;
            final String gender;
            final String fname;
            final String lname;

            try {
                // 1) Convert the json string response into an actual JSON Object
                JSONObject jsonObject = new JSONObject(json);

                // 2) Get the status code
                int statusCode = jsonObject.getInt(OWM_CODE);

                // 3) Check if the HTTP Status Code if it's valid or not
                if (statusCode == SUCCESS_CODE) {
                    // 4) Retrieve the individual bits of data that we need for our Weather model.
                    // 5) Get the city name from the base jsonObject
                    id = jsonObject.getString(OWM_ID);

                    // 6) Get the country name from "sys" object
                    JSONObject sys = jsonObject.getJSONObject(OWM_SYS);
                    country = sys.getString(OWM_COUNTRY);

                    // 7) Get temperature, humidity, and pressure under "main" object
                    JSONObject main = jsonObject.getJSONObject(OWM_MAIN);
                    temperature = main.getDouble(OWM_TEMPERATURE);
                    humidity = main.getLong(OWM_HUMIDITY);
                    pressure = main.getLong(OWM_PRESSURE);

                    // 8) Get description and icon from "weather" object
                    JSONArray weather = jsonObject.getJSONArray(OWM_WEATHER);
                    JSONObject w0 = weather.getJSONObject(0);
                    description = w0.getString(OWM_DESCRIPTION).toUpperCase();
                    icon = w0.getString(OWM_ICON);


                    FbProfile w = new FbProfile()
                            .setBirthday(birthday)
                            .setEmail(email)
                            .setGender(gender)
                            .setFname(fname)
                            .setLname(lname);

                    return w;
                } else {
                    return null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }



}
